import React, {Component} from 'react'
import styled from 'react-emotion'
import Button from './Button'

const Container = styled('div')`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`
const Inputform = styled('input')`
  color: '#000000';
  width: 300px;
  height: 30px;
  font-size: 24px;
  text-align: center;
`
const Title = styled('span')`
  font-size: 30px;
  margin: 10px;
`
const DeleteButton = styled(Button)`
  background-color: red;
  color: black;
  font-size: 15px;
  width: 24px;
  height: 24px;
  padding: 0%;
  border-radius: 50%;
  border: 0px;
  order: 1;
  position: absolute;
  right: 30%;
  cursor: pointer;
`

class Todolist extends Component {
  state = {listTodo: [], textboxComment: ''}

  saveCommentEnt = e => {
    if (e.charCode === 13) {
      this.state.listTodo.push({text: this.state.textboxComment})
      this.setState(() => ({
        listTodo: this.state.listTodo,
        textboxComment: '',
      }))
    }
  }

  getComment = e => {
    const textboxComment = e.target.value
    this.setState(() => ({
      textboxComment,
    }))
  }

  deleteTodo = index => {
    this.state.listTodo.splice(index, 1)
    this.setState(() => ({listTodo: this.state.listTodo}))
  }
  render() {
    const listItems = this.state.listTodo.map((item, index) => (
      <Container key={index}>
        <DeleteButton action={() => this.deleteTodo(index)}>X</DeleteButton>
        <Title>{`${item.text}`}</Title>
      </Container>
    ))
    return (
      <Container>
        <Title>To-do-List </Title>
        <Inputform
          placeholder="try to input some things"
          value={this.state.textboxComment}
          onChange={this.getComment}
          onKeyPress={this.saveCommentEnt}
        />
        {listItems}
      </Container>
    )
  }
}

export default Todolist

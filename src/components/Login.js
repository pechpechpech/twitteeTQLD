import React from 'react'
import styled from 'react-emotion'
import Button from './Button'
import {compose, lifecycle} from 'recompose'
import {connect} from 'react-redux'
import {Redirect} from 'react-router'
import {getIdlogin, getPassword, checkAuth, getDatabase} from '../ducks/twittee'

const Container = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-top: 150px;
`
const Title = styled('span')`
  color: ${props => props.color};
  font-size: 16px;
  margin: 10px;
  display: flex;
`

const Inputform = styled('input')`
  color: '#000000';
  width: 290px;
  height: 25px;
  border: 1px solid #afafa8;
  font-size: 12px;
  padding-left: 7px;
`
const Boderform = styled('div')`
  border: 3px solid #afafa8;
  width: 300px;
  height: 250px;
  display: flex;
  flex-direction: column;
  text-align: left;
  padding: 5px;
`
const Centerform = styled('div')`
  display: flex;
  justify-content: center;
`

const Login = ({
  getIdlogin,
  getPassword,
  checkAuth,
  idlogin,
  password,
  islogin,
  warningtext,
}) => {
  return islogin ? (
    <Redirect to="/Twittee" />
  ) : (
    <Container>
      <Boderform>
        <Title>
          <b>Username</b>
        </Title>

        <Inputform
          onChange={e => getIdlogin(e.target.value)}
          value={idlogin}
          placeholder="Enter Username"
        />

        <Title>
          <b>Password</b>
        </Title>
        <Inputform
          onChange={e => getPassword(e.target.value)}
          value={password}
          type="password"
          placeholder="Enter Password"
        />
        {!warningtext ? (
          <Title color="red">
            <b>User or Pasword is not correct</b>
          </Title>
        ) : null}

        <Centerform>
          <Button
            onClick={() => checkAuth(idlogin, password)}
            bgColor="#FCFC08">
            Login
          </Button>
        </Centerform>
      </Boderform>
    </Container>
  )
}

const mapStateToProps = state => ({
  idlogin: state.twittee.idlogin,
  password: state.twittee.password,
  islogin: state.twittee.islogin,
  warningtext: state.twittee.warningtext,
})
const mapDispatchToProps = dispatch => ({
  getIdlogin: value => dispatch(getIdlogin(value)),
  getPassword: value => dispatch(getPassword(value)),
  checkAuth: (idlogin, password) => dispatch(checkAuth(idlogin, password)),
  getDatabase: () => dispatch(getDatabase(dispatch)),
})

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  lifecycle({
    componentDidMount() {
      this.props.getDatabase()
    },
  }),
)
export default enhance(Login)

import React from 'react'
import styled, {injectGlobal} from 'react-emotion'
import Login from './Login'
import Twittee from './Twittee'
import {connect} from 'react-redux'
import {Route, Link, BrowserRouter} from 'react-router-dom'
import {compose, lifecycle} from 'recompose'
import {logOut} from '../ducks/twittee'

injectGlobal`
  body{margin : 0}
`

const Background = styled('div')`
  background-color: white;
  display: flex;
  justify-content: center;
`
const Menubar = styled('div')`
  background-color: #000;
  height: 50px;
  width: 100%;
  position: fixed;
  z-index: 1;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
`
const TopLeft = styled('div')`
  display: flex;
  flex-direction: row;
`
const TopRight = styled('div')`
  display: flex;
  flex-direction: row;
`
const NavMenu = styled('div')`
  background-color: ${props => props.bgcolor};
  color: white;
  font-size: 20px;
  height: 50px;
  width: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  :hover {
    background-color: #ccc;
    color: black;
  }
`
const NavTop = styled('div')`
  background-color: #fcfc08;
  color: black;
  font-size: 20px;
  height: 50px;
  width: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`
const Toprightform = styled('div')`
  background-color: #fcfc08;
  color: black;
  font-size: 20px;
  height: 50px;
  width: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`
const Textwelcome = styled('span')`
  font-size: 20px;
  color: white;
  margin-top: 9px;
  margin-right: 10px;
`

const Run = ({islogin, idlogin, logOut}) => (
  <BrowserRouter>
    <Background>
      <Menubar>
        {islogin ? (
          <React.Fragment>
            <TopLeft>
              <NavTop>Twittee</NavTop>
              <NavMenu bgcolor="black">Menu</NavMenu>
              <NavMenu bgcolor="black">News</NavMenu>
              <NavMenu bgcolor="black">Contact</NavMenu>
              <NavMenu bgcolor="black">About</NavMenu>
            </TopLeft>
            <TopRight>
              <Textwelcome>{`Welcome Twittee : ${idlogin}`}</Textwelcome>
              <Toprightform>
                <span onClick={() => logOut()}>Logout</span>
              </Toprightform>
            </TopRight>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <TopLeft>
              <NavTop>Twittee</NavTop>
              <NavMenu bgcolor="black">Menu</NavMenu>
              <NavMenu bgcolor="black">News</NavMenu>
              <NavMenu bgcolor="black">Contact</NavMenu>
              <NavMenu bgcolor="black">About</NavMenu>
            </TopLeft>
            <Toprightform>
              <Link to="./Login">Login</Link>
            </Toprightform>
          </React.Fragment>
        )}
      </Menubar>

      <Route path="/Twittee" component={Twittee} />
      <Route path="/Login" component={Login} />
    </Background>
  </BrowserRouter>
)

const mapStateToProps = state => ({
  islogin: state.twittee.islogin,
  idlogin: state.twittee.idlogin,
})
const mapDispatchToProps = dispatch => ({
  logOut: () => dispatch(logOut()),
})

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)
export default enhance(Run)

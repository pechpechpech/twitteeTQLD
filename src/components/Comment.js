import React from 'react'
import styled from 'react-emotion'
import Button from './Button'
import {connect} from 'react-redux'
import {setComment, getComment} from '../ducks/counter'

const Textareaform = styled('textarea')`
  font-style: 'Monospace';
  color: '#000000';
  font-size: 24px;
  width: 400px;
  height: 200px;
  border: 1px solid red;
`
const Inputform = styled('input')`
  color: '#000000';
  width: 200px;
  height: 25px;
  font-size: 20px;
  border: 1px solid red;
`
const StyledShowComment = styled('div')`
  display: flex;
  justify-content: center;
  flex-direction: column;
`
const StyledComment = styled('div')`
  display: flex;
  justify-content: center;
  align-items: center;
`

const Comment = ({
  setComment,
  getComment,
  list,
  textboxComment,
  position,
  nameOfinput,
}) => {
  return (
    <StyledShowComment>
      <Textareaform disabled value={list[position].listComment.join('\n')} />

      <StyledComment>
        {nameOfinput}
        <Inputform
          placeholder="comment user"
          onKeyPress={e => {
            if (e.charCode === 13) {
              setComment(position)
            }
          }}
          onChange={e => getComment(e, position)}
          value={textboxComment[position]}
        />
        <Button
          dis={textboxComment[position] === ''}
          bgColor={!textboxComment[position] ? '#ff0000' : '#3E54BE'}
          onClick={() => setComment(position)}>
          Save
        </Button>
      </StyledComment>
    </StyledShowComment>
  )
}

const mapStateToProps = state => ({
  textboxComment: state.counter.textboxComment,
  list: state.counter.list,
})
const mapDispatchToProps = dispatch => ({
  setComment: index => dispatch(setComment(index)),
  getComment: (e, index) => dispatch(getComment(e, index)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Comment)

import React from 'react'
import Button from './Button'
import styled from 'react-emotion'
import {connect} from 'react-redux'
import {compose, lifecycle, withState} from 'recompose'
import {Redirect} from 'react-router-dom'
import {
  saveComment,
  getComment,
  getDatabase,
  deleteComment,
  editComment,
} from '../ducks/twittee'

import {Input, Button as buttonant, Modal} from 'antd'

const Name = styled('div')`
  font-size: 34px;
  width: 280px;
  text-align: ${props => props.txtalign};
  color: black;
`
const Img = styled('img')`
  border: 1px;
  width: 100px;
  height: 100px;
  border-radius: 50%;
  border: 1px solid black;
  position: relative;
  left: ${props => props.left};
  bottom: ${props => props.bottom};
  right: ${props => props.right};
  top: ${props => props.top};
`

const Inputform = styled(Input.TextArea)``

const Textareaform = styled('div')`
  color: #000000;
  font-size: 24px;
  width: 600px;
  height: 200px;
  border: 1px solid black;
  border-radius: 30px;
  text-align: center;
  padding-top: 50px;
  background-color: ${props => props.bgColor};
  position: relative;
  :hover {
    button {
      display: flex;
    }
  }
  word-break: break-all;
`

const Container = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
const Background = styled('div')`
  background-color: rgba(0, 0, 0, 0.6);
  width: 75%;
`
const ReverseContainer = styled('div')`
  display: flex;
  flex-direction: column-reverse;
  align-items: center;
  justify-content: center;
  padding-top: 50px;
`
const Rowform = styled('div')`
  display: flex;
  flex-direction: row;
  align-items: center;

  .ant-input {
    color: #000000;
    width: 480px;
    font-size: 20px;
    border: 1px solid #afafa8;
    margin-top: 70px;
    margin-bottom: 20px;
    border-radius: 40px;
    text-align: center;
    resize: none;
    min-height: 140px;
  }
`
const ModalStyle = styled(Rowform)`
  .ant-input {
    min-height: 130px;
    margin-top: 10px;
  }
`
const Columnform = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
`
const CloseButton = styled(buttonant)`
  width: 30px;
  height: 30px;
  color: black;
  background-color: red;
  border: 1px solid black;
  border-radius: 50%;
  justify-content: center;
  position: absolute;
  right: -15px;
  top: -12px;
  display: none;
  cursor: pointer;
`

const EditButton = styled(CloseButton)`
  background-color: yellow;
  right: 17px;
`
const SubmitButton = styled(Button)`
  margin-top: 55px;
  :hover {
    transform: scale(1.1);
  }
`

const UserBox = compose(
  withState('modal', 'showModal', false),
  withState('edittext', 'edit', ''),
)(
  ({
    index,
    item,
    editComment,
    deleteComment,
    listUsers,
    modal,
    showModal,
    userid,
    edittext,
    edit,
  }) => (
    <Rowform>
      <Columnform>
        <Name txtalign="right">You</Name>
        <Textareaform bgColor="#F9F940">
          {item.text}
          <CloseButton
            type="danger"
            onClick={() => deleteComment(item.id, index)}>
            X
          </CloseButton>
          <EditButton onClick={() => showModal(true)}>E</EditButton>
          <Modal
            key={item.text}
            title="Edit Text"
            visible={modal}
            onOk={() =>
              editComment(item.id, userid, edittext) && showModal(false)
            }
            onCancel={() => showModal(false)}>
            <ModalStyle>
              <Columnform>
                <Inputform maxlength="140" onChange={e => edit(e.target.value)}>
                  {item.text}
                </Inputform>
                {`charleft :${140 - edittext.length}/140`}
              </Columnform>
            </ModalStyle>
          </Modal>
        </Textareaform>
        <Img
          left="200px"
          bottom="250px"
          src={listUsers[item.userId - 1].imageUrl}
        />
      </Columnform>
    </Rowform>
  ),
)

const OtherBox = ({listUsers, item}) => (
  <Rowform>
    <Columnform>
      <Name txtalign="left">{`${listUsers[item.userId - 1].username}`}</Name>
      <Textareaform bgColor="#B2B298">{item.text}</Textareaform>
      <Img
        right="200px"
        bottom="250px"
        src={listUsers[item.userId - 1].imageUrl}
      />
    </Columnform>
  </Rowform>
)

const Twittee = ({
  listPosts,
  listUsers,
  textbox,
  getComment,
  saveComment,
  deleteComment,
  editComment,
  idlogin,
  islogin,
  userid,
}) => {
  const listItems = listPosts.map((item, index) => (
    <Container key={index}>
      {idlogin === listUsers[item.userId - 1].username ? (
        <UserBox
          index={index}
          item={item}
          userid={userid}
          deleteComment={deleteComment}
          listUsers={listUsers}
          editComment={editComment}
        />
      ) : (
        <OtherBox item={item} listUsers={listUsers} />
      )}
    </Container>
  ))
  return islogin ? (
    <Background>
      <Container>
        <Rowform>
          <Inputform
            placeholder="Post Comment Here"
            onChange={e => getComment(e.target.value)}
            value={textbox}
            maxlength="140"
            onKeyPress={e => {
              if (e.charCode === 13) {
                saveComment(textbox)
              }
            }}
          />
          <Columnform>
            <SubmitButton
              onClick={() => saveComment(textbox)}
              bgColor="#fcfc08">
              Twit!
            </SubmitButton>
            {`charleft :${140 - textbox.length}/140`}
          </Columnform>
        </Rowform>

        <ReverseContainer>{listItems}</ReverseContainer>
      </Container>
    </Background>
  ) : (
    <Redirect to="/Login" />
  )
}

const mapStateToProps = state => ({
  listPosts: state.twittee.listPosts,
  listUsers: state.twittee.listUsers,
  textbox: state.twittee.textbox,
  idlogin: state.twittee.idlogin,
  userid: state.twittee.userid,
  islogin: state.twittee.islogin,
})
const mapDispatchToProps = dispatch => ({
  saveComment: text => dispatch(saveComment(text)),
  getComment: value => dispatch(getComment(value)),
  getDatabase: () => dispatch(getDatabase()),
  deleteComment: (id, index) => dispatch(deleteComment(id, index)),
  editComment: (id, userid, text) => dispatch(editComment(id, userid, text)),
})

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  lifecycle({
    componentDidMount() {
      this.props.getDatabase()
    },
  }),
)
export default enhance(Twittee)

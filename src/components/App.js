import React from 'react'
import Run from './Run'
import {Provider} from 'react-redux'
import store from '../ducks/index'

const App = () => (
  <Provider store={store}>
    <Run />
  </Provider>
)

export default App

import React from 'react'
import styled from 'react-emotion'

const Cusbutton = styled('button')`
  background-color: ${props => props.backgroundColor};
  color: black;
  font-size: 24px;
  width: 120px;
  height: 60px;
  border-radius: 20px;
  text-align: center;
  margin: 7.5px;
  border: 0px;
  cursor: pointer;
  transition: 0.5s;
  :hover {
    transform: scale(1.2);
    transform-origin: 30% 0%;
  }
`

const Button = ({bgColor, onClick, children, dis, className}) => (
  <Cusbutton
    className={className}
    backgroundColor={bgColor}
    disabled={dis}
    onClick={onClick}>
    {children}
  </Cusbutton>
)

export default Button

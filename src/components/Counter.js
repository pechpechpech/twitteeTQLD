import React from 'react'
import Button from './Button'
import Comment from './Comment'
import styled from 'react-emotion'
import {connect} from 'react-redux'
import {setCount, setHidden, setDelete} from '../ducks/counter'
import {compose, lifecycle} from 'recompose'

const Name = styled('div')`
  font-size: 24px;
  margin: 15px;
  color: ${props => props.textColor};
  display: flex;
  justify-content: center;
`
const Img = styled('img')`
  border: 1px;
  width: 200px;
  height: 200px;
  margin-right: 20px;
  border: 1px solid red;
`
const Container = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-top: 50px;
`
const Infoform = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
const Infocontainer = styled('div')`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.bgColor};
  width: 100%;
  position: relative;
  /* align-items: center; */
  &:nth-child(even) {
    background-color: #ccc;
  }
`
const DeleteButton = styled(Button)`
  background-color: red;
  color: black;
  font-size: 15px;
  width: 24px;
  height: 24px;
  padding: 0%;
  border-radius: 50%;
  border: 0px;
  order: 1;
  position: absolute;
  right: 15px;
  top: 15px;
  cursor: pointer;
`

const Counter = ({setCount, count, setHidden, status, list, setDelete}) => {
  // console.log(list.length)
  const listItems = list.map((item, index) => (
    <Infocontainer key={item.email}>
      <DeleteButton onClick={() => setDelete(index)}>X</DeleteButton>
      <Infoform>
        <Img src={item.img} />
        <Name textColor={item.color}>{item.name}</Name>
      </Infoform>
      {item.comment === 'true' ? (
        <Comment position={index} nameOfinput={`${item.name} : `} />
      ) : null}
    </Infocontainer>
  ))
  return (
    <Container>
      {listItems}
      {status === 0 ? (
        <Button onClick={() => setHidden()} bgColor="blue">
          Start
        </Button>
      ) : (
        <div>
          <Button bgColor="#b2d234" onClick={() => setCount('up', '#b2d234')}>
            Like
          </Button>
          <Button bgColor="#ff0000" onClick={() => setCount('down', '#ff0000')}>
            Unlike
          </Button>

          <Name>Num of like : {count} </Name>
        </div>
      )}
    </Container>
  )
}
const mapStateToProps = state => ({
  count: state.counter.count,
  status: state.counter.status,
  list: state.counter.list,
})
const mapDispatchToProps = dispatch => ({
  setCount: (count, color) => dispatch(setCount(count, color)),
  setHidden: () => dispatch(setHidden(dispatch)),
  setDelete: index => dispatch(setDelete(index)),
})

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  lifecycle({
    componentDidMount() {
      this.props.setHidden()
    },
    // componentDidUpdate() {
    //   window.scrollBy(0, 420)
    // },
  }),
)
// export default connect(
//   mapStateToProps,
//   mapDispatchToProps,
// )(Counter)

export default enhance(Counter)

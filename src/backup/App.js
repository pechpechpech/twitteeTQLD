import React from 'react'
import Counter from './Counter'
import styled from 'react-emotion'
import Todolist from './Todolist'
import {Provider} from 'react-redux'
import store from './ducks/index'

const Background = styled('div')`
  background-color: white;
`
const Menubar = styled('div')`
  background-color: #000;
  height: 50px;
  width: 100%;
  margin-bottom: 10px;
  position: fixed;
  z-index: 1;
  display: flex;
  flex-direction: row;
`
const NavMenu = styled('div')`
  background-color: ${props => props.bgcolor};
  color: white;
  font-size: 20px;
  height: 50px;
  width: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  :hover {
    background-color: #ccc;
    color: black;
  }
`
const NavTop = styled('div')`
  background-color: green;
  color: white;
  font-size: 20px;
  height: 50px;
  width: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`

const App = () => (
  <Provider store={store}>
    <Background>
      <Menubar>
        <NavTop bgcolor="green">Tinder</NavTop>
        <NavMenu bgcolor="black">Menu</NavMenu>
        <NavMenu bgcolor="black">News</NavMenu>
        <NavMenu bgcolor="black">Contact</NavMenu>
        <NavMenu bgcolor="black">About</NavMenu>
      </Menubar>
      <Counter />
      <Todolist />
    </Background>
  </Provider>
)

export default App

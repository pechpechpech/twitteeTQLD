import React from 'react'
import styled from 'react-emotion'

const Cusbutton = styled('button')`
  background-color: ${props => props.backgroundColor};
  color: white;
  font-size: 18px;
  width: 100px;
  height: 40px;
  border-radius: 20px;
  text-align: center;
  margin: 7.5px;
  border: 0px;
  cursor: pointer;
  transition: 0.5s;
  :hover {
    transform: scale(1.2);
    transform-origin: 30% 0%;
  }
`

const Button = ({action, bgColor, children, dis, className}) => (
  <Cusbutton
    className={className}
    onClick={action}
    backgroundColor={bgColor}
    disabled={dis}>
    {children}
  </Cusbutton>
)

export default Button

import {createReducer, Creator} from './helper'
import {takeEvery, put, select} from 'redux-saga/effects'
import axios from 'axios'

const UPDATE_COUNT = 'UPDATE_COUNT'
const SET_LIST = 'SET_LIST'
const SET_HIDDEN = 'SET_HIDDEN'
const SET_DELETE = 'SET_DELETE'
const SET_COMMENT = 'SET_COMMENT'
const GET_COMMENT = 'GET_COMMENT'

export const setList = Creator(SET_LIST, 'data')
export const setCount = Creator(UPDATE_COUNT, 'check', 'color')
export const setHidden = Creator(SET_HIDDEN, 'dispatch')
export const setDelete = Creator(SET_DELETE, 'index')
export const setComment = Creator(SET_COMMENT, 'index')
export const getComment = Creator(GET_COMMENT, 'e', 'index')

const initial = {
  count: 0,
  list: [],
  status: 0,
  textboxComment: [],
}

export function* watchSaga() {
  yield takeEvery(UPDATE_COUNT, handleCount)
}

function* handleCount({payload: {check, color}}) {
  const response = yield axios.get('https://randomuser.me/api/')
  const list = yield select(state => state.counter.list)
  const count = yield select(state => state.counter.count)

  if (list.length !== 0) {
    list[list.length - 1].comment = 'true'
    list[list.length - 1].color = color
  }

  axios.baseUrl = ''

  list.push({
    name: response.data.results[0].name.first,
    email: response.data.results[0].email,
    img: response.data.results[0].picture.large,
    color: '#black',
    comment: 'false',
    listComment: [],
  })
  yield put(
    setList({
      list: list,
      count: check === 'up' ? count + 1 : count > 0 ? count - 1 : count,
    }),
  )
}

export default createReducer(initial, state => ({
  // [SET_COUNT]: ({check, color, dispatch}) => {
  //   // console.log('xxxxxxxxxxcount')
  //   if (state.list.length !== 0) {
  //     state.list[state.list.length - 1].comment = 'true'
  //     state.list[state.list.length - 1].color = color
  //   }

  //   axios.baseUrl = ''
  //   axios.get('https://randomuser.me/api/').then(response => {
  //     state.list.push({
  //       name: response.data.results[0].name.first,
  //       email: response.data.results[0].email,
  //       img: response.data.results[0].picture.large,
  //       color: '#black',
  //       comment: 'false',
  //       listComment: [],
  //     })
  //     dispatch(
  //       setList({
  //         list: state.list,
  //       }),
  //     )
  //   })

  //   return {
  //     ...state,
  //     count:
  //       check === 'up'
  //         ? state.count + 1
  //         : state.count > 0
  //           ? state.count - 1
  //           : state.count,
  //     list: [...state.list],
  //   }
  // },
  [SET_LIST]: ({data}) => ({...state, ...data}),

  [SET_HIDDEN]: ({dispatch}) => {
    axios.baseUrl = ''
    axios.get('https://randomuser.me/api/').then(response => {
      state.list.push({
        name: response.data.results[0].name.first,
        email: response.data.results[0].email,
        img: response.data.results[0].picture.large,
        color: '#black',
        comment: 'false',
        listComment: [],
      })
      dispatch(
        setList({
          list: state.list,
        }),
      )
    })
    return {...state, status: 1, list: [...state.list]}
  },
  [SET_DELETE]: ({index}) => {
    state.list.splice(index, 1)
    const newList = [...state.list] /// [a,b,sdf]
    return {
      ...state,
      list: newList,
    }
  },
  [SET_COMMENT]: ({index}) => {
    const newList = state.list[index].listComment
    newList.push(state.textboxComment[index])
    state.textboxComment[index] = ''
    // const listComment = state.list[state.list.length - 1].listComment
    // console.log('this list', state.list[state.list.length - 1].listComment)
    return {
      ...state,
      list: [...state.list],
      textboxComment: [...state.textboxComment],
    }
  },
  [GET_COMMENT]: ({e, index}) => {
    state.textboxComment[index] = e.target.value
    return {...state, textboxComment: [...state.textboxComment]}
  },
}))

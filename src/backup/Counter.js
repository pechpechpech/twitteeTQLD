import React, {Component} from 'react'
import Button from './Button'
import Comment from './Comment'
import axios from 'axios'
import styled from 'react-emotion'
import {connect} from 'react-redux'
import {setText} from './ducks/counter'

const Name = styled('div')`
  font-size: 24px;
  margin: 15px;
  color: ${props => props.textColor};
  display: flex;
  justify-content: center;
`
const Img = styled('img')`
  border: 1px;
  width: 200px;
  height: 200px;
  margin-right: 20px;
  border: 1px solid red;
`
const Container = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-top: 50px;
`
const Infoform = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
const Infocontainer = styled('div')`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.bgColor};
  width: 100%;
  position: relative;
  /* align-items: center; */
  &:nth-child(even) {
    background-color: #ccc;
  }
`
const DeleteButton = styled(Button)`
  background-color: red;
  color: black;
  font-size: 15px;
  width: 24px;
  height: 24px;
  padding: 0%;
  border-radius: 50%;
  border: 0px;
  order: 1;
  position: absolute;
  right: 15px;
  top: 15px;
  cursor: pointer;
`

class Counter extends Component {
  // state = [{count: 0}, {img: 0}, {name: 'name'}, {email: 'email'}]
  state = {count: 0, list: [], status: 0}

  handleCount = check => {
    if (check === 'up') {
      // this.setState(() => this.state.count++)
      if (this.state.list.length !== 0) {
        this.getLastelement().comment = 'true'
      }
      this.setState(p => ({
        count: p.count + 1,
        list: this.state.list,
      }))
      this.saveTolist('#b2d234')
    } else {
      // this.setState(
      // if arrow
      // p => (p.count > 0 ? {count: p.count - 1} : {count: p.count}),
      if (this.state.list.length !== 0) {
        this.getLastelement().comment = 'true'
      }
      this.setState(() => ({
        list: this.state.list,
      }))
      this.saveTolist('#FF0000')
      // )
      // this.setState((p)=> {
      //     if (p.count > 0) {
      //         return {count : p.count-1}
      //     }
      //     else{
      //         return {count : p.count}
      //     }
      // })
    }
    // arrow design function from top to above
    // const f1 = (x) => x < 0 ? (x) : (x + 5)
    // const f2 = (x) => {
    //     return x + 5
    // }
    // const f3 = (p) => ({count : p.count-1})
  }
  getLastelement = () => {
    if (this.state.list.length !== 0) {
      const lastIndex = this.state.list.length - 1
      const lastElement = this.state.list[lastIndex]
      return lastElement
    }
  }
  // how to get response api
  saveTolist = newcolor => {
    axios.baseUrl = ''
    axios.get('https://randomuser.me/api/').then(response => {
      // const newList = this.state.list[this.state.list.length - 1].color = newcolor
      // how to edit array value
      // if (this.state.list.length !== 0) {
      //   const lastIndex = this.state.list.length - 1
      //   const lastEement = this.state.list[lastIndex]
      //   lastEement.color = newcolor
      // }
      if (this.state.list.length !== 0) {
        const lastElement = this.getLastelement()
        lastElement.color = newcolor
      }

      // const checkbgcolor =
      //   this.state.list[this.state.list.length - 1].bgcolor === 'white'
      //     ? '#CCC'
      //     : 'white'

      this.state.list.push({
        name: response.data.results[0].name.first,
        email: response.data.results[0].email,
        img: response.data.results[0].picture.large,
        color: '#000000',
        comment: 'false',
        // bgcolor: checkbgcolor,
      })

      // const newList = [...this.state.list]
      this.setState(() => ({
        list: this.state.list,
        // this.state.list[this.state.list.length - 1].color = newcolor,
      }))
      // window.scrollTo(0, document.body.scrollHeight)
    })
  }

  hidden = () => {
    this.setState({status: 1})
    axios.baseUrl = ''
    axios.get('https://randomuser.me/api/').then(response => {
      this.state.list.push({
        name: response.data.results[0].name.first,
        email: response.data.results[0].email,
        img: response.data.results[0].picture.large,
        color: '#black',
        comment: 'false',
      })
      this.setState(() => ({
        list: this.state.list,
      }))
    })
  }

  deleteform = index => {
    this.state.list.splice(index, 1)
    this.setState(() => ({
      list: this.state.list,
    }))
  }

  render() {
    const listItems = this.state.list.map((item, index) => (
      <Infocontainer
        // bgColor={index % 2 === 0 ? 'white' : '#ccc'}
        key={item.email}>
        <DeleteButton action={() => this.deleteform(index)}>X</DeleteButton>
        <Infoform>
          <Img src={item.img} />
          <Name textColor={item.color}>{item.name}</Name>
          {/* <Name>{item.email}</Name> */}
        </Infoform>

        {item.comment === 'true' ? (
          <Comment nameOfinput={`${item.name} : `} />
        ) : null}
      </Infocontainer>
    ))

    // const this.state.list => [<Img src=]
    return (
      <Container>
        <div>{this.props.text}</div>
        <input
          value={this.props.text}
          onChange={e => this.props.setText(e.target.value)}
        />
        {listItems}

        {/* <Img src={this.state.img} />
        <br />
        <Name>{this.state.name}</Name>
        <br />
        <Name>{this.state.email}</Name>
        <br /> */}
        {this.state.status === 0 ? (
          <Button action={() => this.hidden()} bgColor="blue">
            Start
          </Button>
        ) : (
          <div>
            <Button action={() => this.handleCount('up')} bgColor="#b2d234">
              Like
            </Button>
            <Button bgColor="#ff0000" action={() => this.handleCount()}>
              Unlike
            </Button>

            <Name>Num of like : {this.state.count} </Name>
          </div>
        )}
      </Container>
    )
  }
}
const mapStateToProps = state => ({text: state.counter.text})
const mapDispatchToProps = dispatch => ({
  setText: text => dispatch(setText(text)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Counter)

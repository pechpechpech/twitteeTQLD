import React, {Component} from 'react'
import styled from 'react-emotion'
import Button from './Button'

const Textareaform = styled('textarea')`
  font-style: 'Monospace';
  color: '#000000';
  font-size: 24px;
  width: 400px;
  height: 200px;
  border: 1px solid red;
`
const Inputform = styled('input')`
  color: '#000000';
  width: 200px;
  height: 25px;
  font-size: 24px;
  border: 1px solid red;
`
const StyledShowComment = styled('div')`
  display: flex;
  justify-content: center;
  flex-direction: column;
`
const StyledComment = styled('div')`
  display: flex;
  justify-content: center;
  align-items: center;
`

class Comment extends Component {
  state = {
    listComment: [],
    textboxComment: '',
  }
  getComment = e => {
    // console.log('xxxxxxx', e.target.value)
    const textboxComment = e.target.value
    this.setState(() => ({
      textboxComment,
    }))
    // this.setState(() => ({
    //   textboxComment: e.target.value,
    // }))
  }
  saveCommentBut = () => {
    this.state.listComment.push(this.state.textboxComment)
    this.setState(() => ({
      listComment: this.state.listComment,
      textboxComment: '',
    }))
    // console.log(this.state.listComment)
  }
  saveCommentEnt = e => {
    if (e.charCode === 13) {
      this.state.listComment.push(this.state.textboxComment)
      this.setState(() => ({
        listComment: this.state.listComment,
        textboxComment: '',
      }))
    }
  }

  //   handleInput = e => console.log(e.target.value)
  render() {
    return (
      <StyledShowComment>
        <Textareaform disabled value={this.state.listComment.join('\n')} />
        {/* {this.state.textboxComment === '' ? (
          <Button
            dis={true}
            symbol="Save"
            bgColor="#FF0000"
            action={this.saveComment}
          />
        ) : (
          <Button symbol="Save" bgColor="#3E54BE" action={this.saveComment} />
        )} */}
        <StyledComment>
          <span>{this.props.nameOfinput}</span>
          <Inputform
            onKeyPress={this.saveCommentEnt}
            onChange={this.getComment}
            value={this.state.textboxComment}
          />
          <Button
            dis={this.state.textboxComment === ''}
            bgColor={this.state.textboxComment === '' ? '#ff0000' : '#3E54BE'}
            action={this.saveCommentBut}>
            Save
          </Button>
        </StyledComment>
      </StyledShowComment>
    )
  }
}

export default Comment

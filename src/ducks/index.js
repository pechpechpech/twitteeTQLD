import {createStore, combineReducers, applyMiddleware} from 'redux'
import createSagaMiddleware from 'redux-saga'
import {all} from 'redux-saga/effects'
import counter, {watchSagaCounter} from './counter'
import twittee, {watchSagaTwittee} from './twittee'

const saga = createSagaMiddleware()

export const reducers = combineReducers({counter, twittee})

const store = createStore(reducers, applyMiddleware(saga))

function* rootsaga() {
  yield all([watchSagaTwittee()])
}

saga.run(rootsaga)

export default store

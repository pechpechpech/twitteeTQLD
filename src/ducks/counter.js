import {createReducer, Creator} from './helper'
import {takeEvery, put, select} from 'redux-saga/effects'
import axios from 'axios'

const UPDATE_COUNT = 'UPDATE_COUNT'
const SET_LIST = 'SET_LIST'
const LOADING = 'LOADING'
const DELETE = 'DELETE'
const SAVE_COMMENT = 'SAVE_COMMENT'
const GET_COMMENT = 'GET_COMMENT'

export const setList = Creator(SET_LIST, 'data')
export const setCount = Creator(UPDATE_COUNT, 'check', 'color')
export const setHidden = Creator(LOADING)
export const setDelete = Creator(DELETE, 'index')
export const setComment = Creator(SAVE_COMMENT, 'index')
export const getComment = Creator(GET_COMMENT, 'e', 'index')

const initial = {
  count: 0,
  list: [],
  status: 0,
  textboxComment: [],
}

export function* watchSagaCounter() {
  yield takeEvery(UPDATE_COUNT, handleCount)
  yield takeEvery(LOADING, loading)
  yield takeEvery(DELETE, handleDelete)
  yield takeEvery(GET_COMMENT, getCommentSaga)
  yield takeEvery(SAVE_COMMENT, saveCommentSaga)
}

function* handleCount({payload: {check, color}}) {
  const response = yield axios.get('https://randomuser.me/api/')
  const list = yield select(state => state.counter.list)
  const count = yield select(state => state.counter.count)

  if (list.length !== 0) {
    list[list.length - 1].comment = 'true'
    list[list.length - 1].color = color
  }

  axios.baseUrl = ''

  list.push({
    name: response.data.results[0].name.first,
    email: response.data.results[0].email,
    img: response.data.results[0].picture.large,
    color: '#black',
    comment: 'false',
    listComment: [],
  })
  yield put(
    setList({
      list: list,
      count: check === 'up' ? count + 1 : count > 0 ? count - 1 : count,
    }),
  )
}

function* loading() {
  const response = yield axios.get('https://randomuser.me/api/')
  const list = yield select(state => state.counter.list)

  axios.baseUrl = ''
  list.push({
    name: response.data.results[0].name.first,
    email: response.data.results[0].email,
    img: response.data.results[0].picture.large,
    color: '#black',
    comment: 'false',
    listComment: [],
  })
  yield put(
    setList({
      list: list,
      status: 1,
    }),
  )
}

function* handleDelete({payload: {index}}) {
  const list = yield select(state => state.counter.list)
  list.splice(index, 1)
  const newList = [...list]
  yield put(setList({list: newList}))
}

function* getCommentSaga({payload: {e, index}}) {
  // console.log('etet')
  // console.log('this e ', e, 'this index ', index)
  const textboxComment = yield select(state => state.counter.textboxComment)

  textboxComment[index] = e.target.value

  const newTextboxComment = [...textboxComment]
  yield put(setList({textboxComment: newTextboxComment}))
}

function* saveCommentSaga({payload: {index}}) {
  const list = yield select(state => state.counter.list)

  const listComment = yield select(
    state => state.counter.list[index].listComment,
  )
  const textboxComment = yield select(state => state.counter.textboxComment)
  listComment.push(textboxComment[index])
  textboxComment[index] = ''

  yield put(setList({list: list, textboxComment: [...textboxComment]}))
}

export default createReducer(initial, state => ({
  [SET_LIST]: ({data}) => ({...state, ...data}),
}))

import {createReducer, Creator} from './helper'
import {takeEvery, put, select} from 'redux-saga/effects'
import axios from 'axios'

// import {browserHistory} from 'react-router'

const SET_LIST = 'SET_LIST'
const SAVE_COMMENT = 'SAVE_COMMENT'
const EDIT_COMMENT = 'EDIT_COMMENT'
const DELETE_COMMENT = 'DELETE_COMMENT'
const GET_COMMENT = 'GET_COMMENT'
const GET_IDLOGIN = 'GET_IDLOGIN'
const GET_PASSWORD = 'GET_PASSWORD'
const GET_DATABASE = 'GET_DATABASE'
const CHECK_AUTH = 'CHECK_AUTH'
const LOGOUT = 'LOGOUT'

export const setList = Creator(SET_LIST, 'data')
export const saveComment = Creator(SAVE_COMMENT, 'text')
export const editComment = Creator(EDIT_COMMENT, 'id', 'userid', 'text')
export const deleteComment = Creator(DELETE_COMMENT, 'id', 'index')
export const getComment = Creator(GET_COMMENT, 'value')
export const getIdlogin = Creator(GET_IDLOGIN, 'value')
export const getPassword = Creator(GET_PASSWORD, 'value')
export const getDatabase = Creator(GET_DATABASE)
export const checkAuth = Creator(CHECK_AUTH, 'idlogin', 'password')
export const logOut = Creator(LOGOUT)

const initial = {
  listPosts: [],
  listUsers: [],
  idlogin: '',
  password: '',
  userid: '',
  textbox: '',
  islogin: false,
  warningtext: true,
}

export function* watchSagaTwittee() {
  yield takeEvery(GET_COMMENT, getCommentSaga)
  yield takeEvery(GET_IDLOGIN, getIdloginSaga)
  yield takeEvery(GET_PASSWORD, getPasswordSaga)
  yield takeEvery(SAVE_COMMENT, saveCommentSaga)
  yield takeEvery(EDIT_COMMENT, editCommentSaga)
  yield takeEvery(DELETE_COMMENT, deleteCommentSaga)
  yield takeEvery(GET_DATABASE, loading)
  yield takeEvery(CHECK_AUTH, checkAuthLogin)
  yield takeEvery(LOGOUT, logout)
}

function* logout() {
  yield put(setList({islogin: false, warningtext: true}))
  window.sessionStorage.clear()
}

function* checkAuthLogin({payload: {idlogin, password}}) {
  const listUsers = yield select(state => state.twittee.listUsers)
  var i
  for (i = 0; i < listUsers.length; i++) {
    if (idlogin === listUsers[i].username) {
      if (password === listUsers[i].password) {
        yield put(setList({islogin: true, userid: listUsers[i].id}))
        window.sessionStorage.setItem('isloginstorage', true)
        window.sessionStorage.setItem('idloginstorage', idlogin)
        window.sessionStorage.setItem('iduserstorage', listUsers[i].id)
      }
      yield put(setList({warningtext: false}))
    }
  }
}

function* saveCommentSaga({payload: {text}}) {
  var listPosts = yield select(state => state.twittee.listPosts)
  const userid = yield select(state => state.twittee.userid)
  const responsePosts = yield axios.post('http://localhost:3001/posts', {
    userId: userid,
    text: text,
  })
  listPosts.push(responsePosts.data)

  yield put(setList({listPosts: listPosts, textbox: ''}))
}

function* deleteCommentSaga({payload: {id, index}}) {
  var listPosts = yield select(state => state.twittee.listPosts)
  yield axios.delete(`http://localhost:3001/posts/${id}`)
  listPosts.splice(index, 1)
  const newList = [...listPosts]
  yield put(setList({listPosts: newList}))
}

function* editCommentSaga({payload: {id, userid, text}}) {
  var listPosts = yield select(state => state.twittee.listPosts)
  yield axios.put(`http://localhost:3001/posts/${id}`, {
    userId: userid,
    text: text,
  })
  const responsePosts = yield axios.get('http://localhost:3001/posts')
  listPosts = responsePosts.data

  yield put(setList({listPosts: listPosts}))
}

function* getCommentSaga({payload: {value}}) {
  yield put(setList({textbox: value}))
}
function* getIdloginSaga({payload: {value}}) {
  yield put(setList({idlogin: value}))
}
function* getPasswordSaga({payload: {value}}) {
  yield put(setList({password: value}))
}

function* loading() {
  const responsePosts = yield axios.get('http://localhost:3001/posts')
  const responseUsers = yield axios.get('http://localhost:3001/users')

  var listPosts = yield select(state => state.twittee.listPosts)
  var listUsers = yield select(state => state.twittee.listUsers)
  var getislogin = window.sessionStorage.getItem('isloginstorage')
  var getidlogin = window.sessionStorage.getItem('idloginstorage')
  var getiduser = window.sessionStorage.getItem('iduserstorage')

  axios.baseUrl = ''
  listPosts = responsePosts.data
  listUsers = responseUsers.data
  yield put(
    setList({
      listPosts: listPosts,
      listUsers: listUsers,
      islogin: getislogin,
      idlogin: getidlogin,
      userid: getiduser,
    }),
  )
}

export default createReducer(initial, state => ({
  [SET_LIST]: ({data}) => ({...state, ...data}),
}))
